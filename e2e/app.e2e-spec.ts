import { HermesPage } from './app.po';

describe('hermes App', function() {
  let page: HermesPage;

  beforeEach(() => {
    page = new HermesPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
