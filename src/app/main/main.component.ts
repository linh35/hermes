import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
declare var faker: any;

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  private parentState = 'main';
  selectedTab;
  calendarVisibility = false;
  toggletTabs = false;
  mainLogoUrl;
  companyName;
  companyDescription;
  constructor(private router: Router) {
    this.selectedTab = router.url;

    router.events
      .filter(e => e instanceof NavigationEnd)
      .subscribe((e:any) =>{
        this.selectedTab = e.urlAfterRedirects;
      });
  }

  activeTabIs(childState):boolean {
    let result = this.selectedTab === `/${this.parentState}/${childState}`;
    return result;
  }

  someTabIsActive() {
    let result = this.selectedTab != '';
    return result;
  }

  ngOnInit() {
     this.mainLogoUrl = 'http://media02.hongkiat.com/logo-parodies/kfc_kenpachi-fried-chicken.jpg';
     this.companyName = 'Vindilla'
     this.companyDescription = 'Sub profile of Umbrella Corp.'
  }

  toggleCalendar(isShown:boolean) {
    this.calendarVisibility = isShown;
    console.log("toggle calendar");
  }

  hideCalendar() {
    this.toggleCalendar(false);
  }
}
