import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit  } from '@angular/core';
declare var scheduler;

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit, AfterViewInit  {

  @Output() onClose = new EventEmitter();

  constructor() { }

  ngOnInit() {
     this.initCalendar();
  }


  ngAfterViewInit() {
  }

  initCalendar() {
      scheduler.config.xml_date="%Y-%m-%d %H:%i";
      scheduler.init('app-calendar', new Date(), "month");
      var conferences = [
          { text: "EnyoJS workshop at dotJS",
              start_date: "12.01.2012 14:00", end_date: "12.01.2012 18:00" },
          { text: "CSS Dev Conf",
              start_date: "12.05.2012 09:00", end_date: "12.05.2012 17:00" },
          { text: "RuPy 12 Conf : Brazil",
              start_date: "12.07.2012 08:30", end_date: "12.09.2012 18:00" },
          { text: "Cardiffrb",
              start_date: "12.13.2012 19:30", end_date: "12.13.2012 22:30" },
          { text: "Django User Group Berlin #32",
              start_date: "12.18.2012 19:00", end_date: "12.18.2012 21:00" }
      ];
      scheduler.parse(conferences, 'json');
  }

  close() {
    this.onClose.emit();
  }
}


