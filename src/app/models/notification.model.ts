export class Notification {
    constructor(
      public id,
      public description,
      public companyLogoSrc,
      public type
     ) {}
}
