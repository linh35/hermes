export class Profile {
  constructor(
    public id,
    public description,
    public picUrl,
    public location,
    public categories: any[]
  ) {}
}
