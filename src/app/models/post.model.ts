export class Post {
  constructor(
    public id,
    public description,
    public picUrl,
    public videoUrl,
    public isLiked: boolean,
    public likesCount,
    public categories: any[],
    public location,
    public title
  ) {}
}
