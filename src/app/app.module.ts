import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { TabsListComponent } from './tabs-list/tabs-list.component';
import { MainComponent } from './main/main.component';
import { TabComponent } from './tab/tab.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import {AuthenticationService} from './shared/authentication.service';
import {HttpService} from './shared/http.service';
import { EmptyComponent } from './empty/empty.component';

import { routesConfig } from './app.routes-config';
import { StringFilter } from './shared/string.filter';
import { SeeComponent } from './see/see.component';
import { PostActionsComponent } from './posts/post-actions/post-actions.component';
import { NewsBoardComponent } from './news/news-board.component';
import { CalendarComponent } from './calendar/calendar.component';
import {CalendarService} from 'app/shared/calendar.service';
import {PostsService} from 'app/shared/posts.service';
import { NotificationsService } from 'app/shared/notifications.service';
import { ProfileService } from 'app/shared/profile.service';


import { HearComponent } from './hear/hear.component';
import { SayComponent } from './say/say.component';
import { CreateEditPostComponent } from './posts/create-edit-post/create-edit-post.component';
import { PostBodyComponent } from './posts/post-body/post-body.component';
import { BoardComponent } from './posts/board/board.component';
import { OurPostComponent } from './posts/our-post/our-post.component';
import { GalleryPostComponent } from './posts/gallery-post/gallery-post.component';
import { GalleryBoardComponent } from './posts/board/gallery-board.component';
import { NotificationComponent } from './notifications/notification/notification.component';
import { NotificationListComponent } from './notifications/notification-list/notification-list.component';
import { SidePostComponent } from './posts/side-post/side-post.component';
import { SideBoardComponent } from './posts/board/side-board.component';
import { PreviousPostsComponent } from './say/previous-posts.component';
import { PopupPostComponent } from './posts/popup-post/popup-post.component';
import { PopupProfileComponent } from './profiles/popup-profile/popup-profile.component';
import { ProfileBodyComponent } from './profiles/profile-body/profile-body.component';

import { Ng2DraggableModule } from 'ng2-draggable';
import { ClickOutsideModule } from 'ng-click-outside';

import { MenuComponent } from './tabs-list/menu/menu.component';
import { PersonalProfileComponent } from './personal-profile/personal-profile.component';

@NgModule({
  declarations: [
    TabsListComponent,
    AppComponent,
    MainComponent,
    TabComponent,
    DashboardComponent,
    EmptyComponent,
    SeeComponent,
    PostActionsComponent,
    NewsBoardComponent,
    CalendarComponent,
    HearComponent,
    SayComponent,
    CreateEditPostComponent,
    StringFilter,
    PostBodyComponent,
    BoardComponent,
    OurPostComponent,
    GalleryPostComponent,
    GalleryBoardComponent,
    NotificationComponent,
    NotificationListComponent,
    SidePostComponent,
    SideBoardComponent,
    PreviousPostsComponent,
    PopupPostComponent,
    PopupProfileComponent,
    ProfileBodyComponent,
    MenuComponent,
    PersonalProfileComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    routesConfig,
    Ng2DraggableModule,
    ClickOutsideModule
  ],
  providers: [AuthenticationService, HttpService, PostsService, CalendarService, NotificationsService, ProfileService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
