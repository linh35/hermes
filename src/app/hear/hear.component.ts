import { Component, OnInit } from '@angular/core';
import {PostsService} from 'app/shared/posts.service';
import {ProfileService} from 'app/shared/profile.service';
import {Post} from 'app/models/post.model';

@Component({
  selector: 'app-hear',
  templateUrl: './hear.component.html',
  styleUrls: ['./hear.component.scss']
})
export class HearComponent implements OnInit {
  originalPosts = [];
  posts = [];
 

  constructor(private postsService: PostsService) {}

  ngOnInit() {
    this.postsService.getPosts()
      .subscribe(data => {
        this.posts = data;
      });
  }

}
