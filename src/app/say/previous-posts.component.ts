import { Component, OnInit } from '@angular/core';
import {Post} from 'app/models/post.model';
import { PostsService } from 'app/shared/posts.service';

@Component({
  selector: 'app-previous-posts',
  templateUrl: './previous-posts.component.html',
  styleUrls: ['./previous-posts.component.scss']
})
export class PreviousPostsComponent implements OnInit {
  posts: Post[] = [];
  constructor(private postsService: PostsService) { }

  ngOnInit() {
    this.postsService.getPreviousPosts()
      .subscribe(data => {
        this.posts = data;
      });
  }
}
