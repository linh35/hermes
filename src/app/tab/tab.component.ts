import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss']
})
export class TabComponent {

  @Input('state') state: string;

  @Input('label') label: string;

  @Input() isActive: boolean;
  constructor() { }
  isActiveFunc() {
    return this.isActive;
  }
  ngOnInit() {
  }
}
