import { Component, OnInit } from '@angular/core';
import {PostsService} from 'app/shared/posts.service';
import {ProfileService} from 'app/shared/profile.service';
import {Post} from 'app/models/post.model';

@Component({
  selector: 'app-see',
  templateUrl: './see.component.html',
  styleUrls: ['./see.component.scss']
})
export class SeeComponent implements OnInit {
  posts = [];
  profiles = [];
  searchProfiles: boolean = false;

  constructor(private postsService: PostsService, private profileService: ProfileService) {}

  ngOnInit() {
    this.postsService.getPosts()
      .subscribe(data => {
        this.posts = data;
      });

    this.profileService.getProfiles()
      .subscribe(data=> {
        this.profiles = data;
      });
  }

  onSelectionChange(newSearchType) {
    this.searchProfiles = newSearchType === "profiles";
  }
}
