import { Injectable } from '@angular/core';
import {HttpService} from './http.service';
import {Post} from 'app/models/post.model';
import { Observable, Subject } from "rxjs/Rx";
import {toObservable} from 'app/shared/to-observable.function';
import * as _ from 'lodash';

declare var faker;

@Injectable()
export class PostsService {

  postPopUp = new Subject<any>();
  public getPopUpPost() : Observable<any> {
    return this.postPopUp.asObservable();
  }
  //temp
  public posts = [];

  constructor(private http: HttpService) {
    let amazonLogo = "https://www.seeklogo.net/wp-content/uploads/2016/10/amazon-logo-preview.png";
    let googleLogo = 'https://i.kinja-img.com/gawker-media/image/upload/s--pEKSmwzm--/c_scale,fl_progressive,q_80,w_800/1414228815325188681.jpg';
    let nokiaLogo = 'https://forum.ge/avatars/av-94596.png';
    let categories = ['food', 'delivery', 'rent', 'realestate', 'pr', 'marketing', 'sales', 'cars'];

    this.posts.push(new Post(2, faker.lorem.sentences(3), amazonLogo, '', true, 5, _.sampleSize(categories, 2), 'Sweden', 'Promotion'));
    this.posts.push(new Post(1, faker.lorem.sentences(3), nokiaLogo, '', true, 5, _.sampleSize(categories, 2), 'Bulgaria', 'One time offer!'));
    this.posts.push(new Post(1, faker.lorem.sentences(3), googleLogo, '', false, 15, _.sampleSize(categories, 2), 'England', 'The legend continues..'));
    this.posts.push(new Post(1, faker.lorem.sentences(2), nokiaLogo, '', false, 5, _.sampleSize(categories, 2), 'Germany', 'Get it only now!'));
    this.posts.push(new Post(2, faker.lorem.sentences(4), amazonLogo, '', true, 4, _.sampleSize(categories, 2), 'Alaska', 'Half price for all deliverables'));
    this.posts.push(new Post(1, faker.lorem.sentences(5), googleLogo, '', false, 5, _.sampleSize(categories, 2), 'Australia', 'Nexus 10'));
    this.posts.push(new Post(2, faker.lorem.sentences(2), amazonLogo, '', false, 0, _.sampleSize(categories, 2), 'Denmark', 'Kindle 11'));
  }

  public getPosts() {
    return toObservable(this.posts);
  }

  public getAnnouncements() {
    return toObservable(this.posts);
  }

  public getPreviousPosts() {
    var picUrl = faker.image.business();
    let previousPosts = this.posts.filter(function(post) {
      return post.id == 2; // hardcored amazon logo
    });
    return toObservable(this.getDuplicateArray(previousPosts));
  }

  public popUpPost(post: Post) {
    this.postPopUp.next(post);
  }

  public closePopUp() {
    this.postPopUp.next();
  }

  public getDuplicateArray(fromArray) {
    let arr = [];
    fromArray.forEach((x) => {
      arr.push(Object.assign({}, x));
    });
    return arr;
  }

}
