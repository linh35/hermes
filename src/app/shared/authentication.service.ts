import { Injectable } from '@angular/core';
import {HttpService} from './http.service';
import { Response, Headers, URLSearchParams } from '@angular/http';
import 'rxjs/Rx';
import { Observable, Subject } from "rxjs/Rx";

@Injectable()
export class AuthenticationService {

  constructor(private http: HttpService) { }


  public register() {

  }

  public login() {

  }

  public logout() {

  }

  public getUserInfo() {
    return this.http.get("user/getUserInfo")
    .map((response: Response) => response.json());
  }

}
