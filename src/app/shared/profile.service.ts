import { Injectable } from '@angular/core';
import { Observable, Subject } from "rxjs/Rx";
import {toObservable} from 'app/shared/to-observable.function';
import { Profile } from 'app/models/profile.model';
declare var faker;

@Injectable()
export class ProfileService {

  profiles: Profile[] = [];
  profilePopUp = new Subject<any>();
  public getPopUpProfile() : Observable<any> {
    return this.profilePopUp.asObservable();
  }
  constructor() {
    let logoUrl = 'https://vignette3.wikia.nocookie.net/villains/images/8/8a/The_Umbrella_Logo.jpg/revision/latest?cb=20140510225737';
    let logoUrl1 = 'http://www.thelogomix.com/files/imagecache/v3-logo-detail/e400.gif';
    let logoUrl2 = 'https://stocklogos.com/sites/default/files/tumblr_lpgq8fjbov1qfw54v.jpg';
    let logoUrl3 = 'https://dcassetcdn.com/design_img/733372/126762/126762_4466777_733372_image.png';
    let logoUrl4 = 'http://sliptalk.s3-us-west-2.amazonaws.com/wp-content/uploads/2014/10/06154222/boundary.png';

    let categories = ['delivery', 'food']
    let categories2 = ['development', 'rent']
    let categories3 = ['architecture', 'building materials']

    this.profiles.push(new Profile(99, faker.lorem.sentences(10), logoUrl, 'Texas, USA', categories ));
    this.profiles.push(new Profile(95, faker.lorem.sentences(6), logoUrl1, 'Maryland, USA', categories2 ));
    this.profiles.push(new Profile(98, faker.lorem.sentences(5), logoUrl2, 'Arizona, USA', categories2 ));
    this.profiles.push(new Profile(97, faker.lorem.sentences(7), logoUrl3, 'Maryland, USA', categories3 ));
    this.profiles.push(new Profile(96, faker.lorem.sentences(6), logoUrl4, 'Alaska, USA', categories ));
  }


  popUpProfile(notificationId) {
    // TODO: logic to get profile from notification id
    this.profilePopUp.next(this.profiles[0]);
  }

  getProfiles() {
    return toObservable(this.profiles);
  }
}
