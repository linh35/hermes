import { Component, OnInit, Input } from '@angular/core';
import { Profile } from 'app/models/profile.model';

@Component({
  selector: 'app-profile-body',
  templateUrl: './profile-body.component.html',
  styleUrls: ['./profile-body.component.scss']
})
export class ProfileBodyComponent implements OnInit {

  @Input() profile: Profile;
  constructor() { }

  ngOnInit() {
  }

}
