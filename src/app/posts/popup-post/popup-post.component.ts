import { Component, OnInit } from '@angular/core';
import { PostsService } from 'app/shared/posts.service';
import {Post} from 'app/models/post.model';

@Component({
  selector: 'app-popup-post',
  templateUrl: './popup-post.component.html',
  styleUrls: ['./popup-post.component.scss']
})
export class PopupPostComponent implements OnInit {
  post: Post;
  constructor(private postsService: PostsService) { }

  ngOnInit() {
    this.postsService.getPopUpPost().subscribe((data)=>{
      this.togglePopUp(data);
    });
  }

  togglePopUp(data:any) {
    if(data) {
      this.post = data;
    }
    else{
      this.post = null;
    }
  }

}
