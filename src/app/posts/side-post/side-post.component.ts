import { Component, OnInit, Input } from '@angular/core';
import {Post} from 'app/models/post.model';
import { PostsService } from 'app/shared/posts.service';

@Component({
  selector: 'app-side-post',
  templateUrl: './side-post.component.html',
  styleUrls: ['./side-post.component.scss']
})
export class SidePostComponent implements OnInit {

   @Input() post: Post;
   fullPostDescription: string = '';
  constructor(private postsService: PostsService) { }

  ngOnInit() {
    this.fullPostDescription = this.post.description;
    this.post.description = this.post.description.substring(0, 40) + '...';
  }

  popUpPost() {
    let tempPost = new Post(
      this.post.id,
      this.fullPostDescription,
      this.post.picUrl,
      this.post.videoUrl,
      this.post.isLiked,
      this.post.likesCount,
      this.post.categories,
      this.post.location,
      this.post.title
    );

    this.postsService.popUpPost(tempPost);
  }

}
