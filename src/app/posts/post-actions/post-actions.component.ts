import { Component, OnInit, Input } from '@angular/core';
import {Post} from 'app/models/post.model';
import { NotificationsService } from 'app/shared/notifications.service';

@Component({
  selector: 'post-actions',
  templateUrl: './post-actions.component.html',
  styleUrls: ['./post-actions.component.scss']
})
export class PostActionsComponent implements OnInit {

  @Input() post: Post;
  constructor(private notificationService: NotificationsService) { }

  ngOnInit() {

  }

  toggleLike() {
    if(this.post.isLiked) {
      this.post.likesCount--;
    }
    else{
      this.post.likesCount++;
      setTimeout(function() {
        this.notificationService.add();
      }.bind(this), 3000);
    }
    this.post.isLiked = !this.post.isLiked;
  }
}
