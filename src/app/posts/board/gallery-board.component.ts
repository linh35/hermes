import { Component, OnInit, Input } from '@angular/core';
import {Post} from 'app/models/post.model';

@Component({
  selector: 'gallery-board',
  templateUrl: './gallery-board.component.html',
  styleUrls: ['./gallery-board.component.scss']
})
export class GalleryBoardComponent implements OnInit {
  @Input() posts: Post[];
   constructor() { }

  ngOnInit() {
  }

}
