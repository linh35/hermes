import { Component, OnInit, Input } from '@angular/core';
import {Post} from 'app/models/post.model';

@Component({
  selector: 'app-side-board',
  templateUrl: './side-board.component.html',
  styleUrls: ['./side-board.component.scss']
})
export class SideBoardComponent implements OnInit {

  @Input() posts: Post[] = [];
  constructor() { }

  ngOnInit() {
  }

}
