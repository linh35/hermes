import { Component, OnInit, OnDestroy, Input, OnChanges, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import {FormsModule, NgForm} from "@angular/forms";
import {CreateEditPost} from 'app/models/create-edit-post.model';
import {PostsService} from 'app/shared/posts.service';
import {Post} from 'app/models/post.model';


@Component({
  selector: 'app-create-edit-post',
  templateUrl: './create-edit-post.component.html',
  styleUrls: ['./create-edit-post.component.scss']
})
export class CreateEditPostComponent implements OnInit, OnDestroy, OnChanges {

  @Input() editPost;
  uploadedImageSrc: string = '';
  post: Post;
  editor;
  editMode: boolean = false;
  totalPrice = '';
  finalState = false;
  navigationText = 'Next';
  addedCategories: string[] = [];
  availableCategories = [
    'restaurant franchize',
    'ISP', 'software  development',
    'delivery service', 'networking',
    'PR', 'human resources', 'marketing',
    'security', 'healthcare', 'medicine',
    'pharmacy', 'finances', 'personal finances',
    'life coaching', 'social networking'
  ].map(cat => { return { selected: false, name: cat }; });

  @ViewChild('fileUpload') fileUpload;

  public heading: string;
  constructor(private postsService: PostsService) { }

  ngOnInit() {
    if(this.editMode) {
      this.heading = 'Edit';
    }else{
      this.heading = 'Create';
    }

    if(this.editPost!= null) {
      this.mapToEditPost(this.editPost);
    }
    else{
      this.post = new Post(1, '', '', '', false, 0, [], '', '');
    }
  }

  mapToEditPost(newPost) {
    this.post.id = newPost.id;
    this.post.description = newPost.description;
    this.post.picUrl = newPost.picUrl;
    this.post.videoUrl = newPost.videoUrl;
    this.post.title = 'test';

    if(this.post.description !=undefined) {
          this.editor.setContent(this.post.description);
      }
  }

  onfileUpload(event) {
    let  files = event.srcElement.files;
    if(files) {
       let  reader = new FileReader();
       reader.onload = function (e:any) {
         this.uploadedImageSrc = e.target.result;
      }.bind(this);

       reader.readAsDataURL(files[0]);
    }

  }

  ngOnChanges(changes) {
    if(changes.editPost.currentValue !== undefined) {
      this.mapToEditPost(changes.editPost.currentValue);
    }
  }
  ngOnDestroy() {
    //tinymce.remove(this.editor);
  }
  toggleCategory(category:any) {
    category.selected = !category.selected;
  }

  onSubmit(model) {
    let selectedCategories = this.availableCategories
      .filter(cat => cat.selected)
      .map(cat => cat.name);

    this.postsService.posts.push(
      new Post(99, model.description, this.uploadedImageSrc, '', false, 0, selectedCategories, model.location, model.title)
    );
  }

  changeState() {
    this.finalState = !this.finalState;
    if(this.navigationText == 'Next') {
      this.navigationText = 'Back';
    }
    else{
      this.navigationText = 'Next';
    }
  }

  chooseFile() {
    this.fileUpload.nativeElement.click();
  }

  calculatePrice() {
    this.totalPrice = '$5.50';
  }
}
