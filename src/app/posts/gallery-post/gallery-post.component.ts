import { Component, OnInit, Input } from '@angular/core';
import { Post } from 'app/models/post.model';
import { PostsService } from 'app/shared/posts.service';

@Component({
  selector: 'gallery-post',
  templateUrl: './gallery-post.component.html',
  styleUrls: ['./gallery-post.component.scss']
})
export class GalleryPostComponent implements OnInit {

  @Input() post: Post;
  constructor(private postsService: PostsService) { }

  ngOnInit() {
  }

  cutDescription(desc) {
    let maxLen = 120;
    let subStr = desc.length > maxLen ? desc.substring(0, maxLen) + '...' : desc;
    return subStr;
  }

  popUpPost() {
    this.postsService.popUpPost(this.post);
  }
}
