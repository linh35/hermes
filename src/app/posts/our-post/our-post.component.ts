import { Component, OnInit, Input } from '@angular/core';
import {Post} from 'app/models/post.model';

@Component({
  selector: 'our-post',
  templateUrl: './our-post.component.html',
  styleUrls: ['./our-post.component.scss']
})
export class OurPostComponent implements OnInit {

  @Input() post: Post;
  constructor() { }

  ngOnInit() {
  }

}
