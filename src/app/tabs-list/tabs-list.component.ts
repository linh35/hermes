import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { tabsConfig } from 'app/app.tabs.config';

@Component({
  selector: 'app-tabs-list',
  templateUrl: './tabs-list.component.html',
  styleUrls: ['./tabs-list.component.scss']
})

export class TabsListComponent implements OnInit {
  tabs:any = tabsConfig;
  showActions: boolean = false;
  @Input() parentState:string;
  @Input() activeTab:string;

  constructor() {}

  ngOnInit() {}

  toggleActions() {
    if(this.showActions) {
      this.showActions = false;
    }
    else{
      this.showActions = true;
    }
  }
}
