import { Component, OnInit, ViewChild } from '@angular/core';
import {Post} from 'app/models/post.model';
import {PostsService} from 'app/shared/posts.service';
declare var faker: any;

@Component({
  selector: 'app-news-board',
  templateUrl: './news-board.component.html',
  styleUrls: ['./news-board.component.scss']
})
export class NewsBoardComponent implements OnInit {
  posts: Post[] = [];
  @ViewChild("postDescription") postDescription;

  constructor(private postsService: PostsService) {

  }

  mainLogo: string;
  ngOnInit() {
    this.mainLogo = faker.image.business();
    this.postsService.getPosts().subscribe((data)=>{ Object.assign(this.posts, data); });
  }

  addPost() {
    let desc = this.postDescription.nativeElement.value;
    let post = new Post(this.posts.length, desc, 'http://diylogodesigns.com/blog/wp-content/uploads/2016/04/airline-company-logo-design-idea.png', '', false, 0, [], 'Sweden', '');
    this.posts.splice(0, 0, post);
  }
}
