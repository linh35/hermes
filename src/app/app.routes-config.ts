import { MainComponent } from './main/main.component';
import { RouterModule } from '@angular/router';
import { EmptyComponent } from 'app/empty/empty.component';

export var routesConfig = RouterModule.forRoot([
    {
      path: '',
      redirectTo: '/main/news',
      pathMatch: 'full'
    },
    {
        path: 'main',
        component: MainComponent,
        children: [
          {
            path: 'news',
            component: EmptyComponent
          },
          {
            path: 'see',
            component: EmptyComponent
          },
          {
            path: 'say',
            component: EmptyComponent
          },
          {
            path: 'hear',
            component: EmptyComponent
          },
          {
            path: 'profile',
            component: EmptyComponent
          }
        ]
    }
]);
