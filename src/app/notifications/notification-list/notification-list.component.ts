import { Component, OnInit, OnDestroy } from '@angular/core';
import { NotificationsService } from 'app/shared/notifications.service';
import { Subscription } from "rxjs/Rx";

@Component({
  selector: 'app-notification-list',
  templateUrl: './notification-list.component.html',
  styleUrls: ['./notification-list.component.scss']
})
export class NotificationListComponent implements OnInit, OnDestroy {
  notifications: any = [];
  constructor(private notificationsService: NotificationsService) { }
  newNotificationSub: Subscription;

  ngOnInit() {
    this.notificationsService.getAll().subscribe((data)=>{
       Object.assign(this.notifications, data);
    });

    this.newNotificationSub =  this.notificationsService.getNotification()
                                                        .subscribe(notification =>
                                                                  this.notifications.push(notification));
  }

  ngOnDestroy() {
        this.newNotificationSub.unsubscribe();
    }

  removeNotification(notification:any) {
    this.notifications = this.notifications.filter((e)=>{ return e.id != notification.id});
  }
}
